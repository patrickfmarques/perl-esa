package ESA;

use 5.010;
use strict;
use warnings FATAL => 'all';

our $VERSION = '0.0.5';

1;

__END__

=head1 NAME

ESA - ElasticSearch Administration

=head1 VERSION

Version 0.0.5

=head1 DESCRIPTION

A command line lib and tool for managing elasticsearch indices.

=head2 INSTALL

Recommended is to use CPANMinus. To install CPANMinus, as root, do:

	curl -L http://cpanmin.us | perl - --sudo App::cpanminus

After that clone CLAP and in its root folder type:

	sudo cpanm .

That's it.

=head1 AUTHORS

J.B. Ribeiro, E<lt>vredens@gmail.comE<gt>

=head1 COPYRIGHT

Copyright (C) 2013-2015, EerieSoftronics

=cut
