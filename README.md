# NAME

ESA - ElasticSearch Administration

# VERSION

Version 0.0.4

# DESCRIPTION

A command line lib and tool for managing elasticsearch indices. The main features are

- View indice settings
- Change indice settings
- Backup an indice into JSON files
- Restore a backup
- Restore a backup into a different index and/or type
- Reindex all documents or all documents of a given type into the same index or a different index
- Supports plugins which can be used to change documents before they are indexes when using the restore or reindex commands

# SYNOPSIS

## Snapshots

**TODO**

## Change index settings

    esa [OPTIONS] set --index <src index> --no-writes --replicas 2 --refresh-interval -1
    esa [OPTIONS] set --index <src index> --writes --replicas 1 --refresh-interval 1

## Create indexes

    esa [OPTIONS] create  --index <index> --mfolder <path> [--type <type>]

The `mfolder` path must be a folder containing a `mappings.json` and `settings.json`
files.

TODO: add link to mappings and settings example files.

## Backup and Restore to a different index

    esa [OPTIONS] backup  --index <src index> --folder <path> [--type <type>]
    esa [OPTIONS] restore --index <dst index> --folder <path> [--type <type>] [--plugin <plugin>]

## Reindex without Backup

    esa [OPTIONS] reindex --src-index <src index> --dst-index <dst index> [--type <type>]

## Reindex to the same index using a plugin to change documents (DataRepair)

    esa [OPTIONS] reindex --index <index> --plugin ESA_main.pl [--type <type>]

## Alias Management

    esa [OPTIONS] alias del --index <old index> --alias <alias>
    esa [OPTIONS] alias put --index <new index> --alias <alias> [--query <json query>]
    esa [OPTIONS] alias move --src-index <old index> --dst-index <new index> --alias <alias> [--query <json query>]

Note that you can not index against aliases which point to more than one index or which have queries associated. So if you are using aliases to version indices keep that in mind.

# OPTIONS

## --host <host>

The elasticsearch host to connect to. It must be an HTTP interface but you can omit the full URL. For example: `--host localhost:9200`.

## --verbose

This flag activates verbose mode.

## --man

The full manual page.

## --help

A quick help message.

## --index <index name>

This "option" is required by most actions and it indicates the index where to do the requested action.

## --src-index <source index>

This "option" is required by some actions when there is the notion of an index which is either being replaced or removed. In some actions, which use only one index, sometimes support passing this options instead of `--index`. For example, doing a backup of an index.

## --dst-index <destination index>

Same as the `--src-index`, this option is used to indicate an index which is either being created or updated.

## --type <index type>

This option indicates the Elasticsearch document type.

## --mfolder <path to folder>

The mappings folder must point to a folder with 2 files: `settings.json` and `mappings.json`. The first must contain an index settings description. The `mappings.json` must contain all types and mappings.

Sample `settings.json`

    {
      "number_of_shards": 5,
      "index": {
        "analysis": {
          "analyzer": {
            "default": {
              "tokenizer": "standard",
              "filter": ["lowercase", "word_delimiter", "icu_folding"]
            },
            "myphonetica": {
              "tokenizer": "standard",
              "filter": ["standard", "lowercase", "myphonetic"]
            }
          },
          "filter": {
            "myphonetic": {
              "type": "phonetic",
              "encoder": "nysiis"
            }
          }
        }
      }
    }

Sample `mappings.json`

    {
      "user": {
        "_meta": {
          "version": "0.1.0"
        },
        "properties": {
          "username": { "type": "string" }
        }
      },
      "tweet": {
        "_meta": {
          "version": "0.1.0"
        },
        "_parent": {
          "type": "user"
        },
        "properties": {
          "text":  { "type": "string" },
          "timestamp": { "type": "date" }
        }
      }
    }

## --plugin <plugin>

See the plugins section.

## --query <json query>

This option, used by the alias management, is used to provide queries to filter index documents. See the appropriate Elasticsearch documentation on how to use them.

# Plugins

ESA supports the use of plugins in order to change documents before an indexing
operation. All plugins must be in Perl and must export an `ESA_main` sub. This
function receives two arguments, a `meta` hashref and a `doc` hashref

Sample `meta`

    {
      _index => 'index',
      _type => 'type',
      _id => 'doc id',
    }

The `doc` is the document stored in elasticsearch (what is stored in the
`_source`).

The `ESA_main` can change the `doc` hashref as it sees fit and it must return
an integer value with the following meaning:

- If it is less than 0 the document is invalid and should not be indexed
- If it is equal to 0 then the document was not changed in any way, yet it
can still be indexed.
- If it greater than 0 then the document was changed and the new version can
be indexed.

Here is a quick `esa-sample.pl` which convers a previous plain text field into
an MD5 of the same value.

## esa-sample.pl

    use Digest::MD5 qw/md5_hex/;
    sub ESA_main {
      my ($meta, $doc) = @_;
      my $r = 0;
      unless (defined $doc->{'new-field'}) {
        $doc->{'new-field'} = md5sum($doc->{'old-field'});
        delete $doc->{'old-field'};
        $r = 1; # we changed the document so we set the output to 1
      }
      return -1 if $doc->{'invalid'};
      return $r;
    }
    1; # requires are interpreted, so we need an exit value ;)

# INSTALL

Recommended is to use CPANMinus. To install CPANMinus, as root, do:

    curl -L http://cpanmin.us | perl - --sudo App::cpanminus

After that clone ESA and in its root folder type:

    sudo cpanm .

That's it.

## Ubuntu/Debian

You need some extra packages which probably don't come with your dist.

    apt-get install libssl-dev

# AUTHORS

J.B. Ribeiro, <vredens@gmail.com>

# COPYRIGHT

Copyright (C) 2015, EerieSoftronics
